/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import domain.Dapp;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class OpenCSVReaderTest {
    
    OpenCSVReader openCSVReader;
    ArrayList<Dapp> dapps;
    
    @Before
    public void init(){
        openCSVReader = new OpenCSVReader();
        dapps = new ArrayList<>();
    }
    
    @Test
    public void TestErrMessage(){
        openCSVReader.setErrMsg("x");
        assertEquals("x", openCSVReader.getErrMsg());
    }
    @Test
    public void createDappListFromCSVTestCorrectFileWorking(){
        dapps = openCSVReader.createDappListFromCSV("Processed\\WasDapp.csv");
        assertNotNull(dapps);
    }
    
    @Test
    public void createDappListFromCSVTestSkipsFirstLine(){
        dapps = openCSVReader.createDappListFromCSV("Processed\\WasDapp.csv");
        assertNotEquals("Titel", dapps.get(1).getTitel());
    }
    
    @Test
    public void createDappListFromCSVTestFaultyFileExcepted() {
        dapps = openCSVReader.createDappListFromCSV("Rejected\\WatSapp.csv");
        assertNull(dapps);
    }
    
    @Test
    public void createDappListFromCSVTestEmptyShowsEmpty(){
        dapps = openCSVReader.createDappListFromCSV("Rejected\\WasDappLeeg.csv");
        assertNull(dapps);
        assertEquals(openCSVReader.getErrMsg(), "File without headers!");
    }
    
    @Test
    public void createDappListFromCSVTestNoHeadersMsgCorrect(){
        dapps = openCSVReader.createDappListFromCSV("Rejected\\WasDappZonderHeaders.csv");
        assertNull(dapps);
        assertEquals(openCSVReader.getErrMsg(),"File without headers!");
    }
    
    @Test
    public void createDappListFromCSVTestMissingTitle(){
        dapps = openCSVReader.createDappListFromCSV("Rejected\\WasDappZonderTitel.csv");
        assertNull(dapps);
        assertEquals(openCSVReader.getErrMsg(),"Empty title in csv-file found.");
    }
    
    @Test
    public void createDappListFromCSVTestValueTooLong(){
        dapps = openCSVReader.createDappListFromCSV("Rejected\\WasDappWaardeTeLang.csv");
        assertNull(dapps);
        assertEquals(openCSVReader.getErrMsg(),"Please keep the maximum length of the values to no more than 256.");
    }

}
