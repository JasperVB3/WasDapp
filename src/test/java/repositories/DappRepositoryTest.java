package repositories;

import Exceptions.NoQueryPossibleException;
import domain.Dapp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DappRepositoryTest {

    private DappRepository dappRepository;
    private static String testURL = "jdbc:mysql://localhost:3306/wasdapptestdb?autoReconnect=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    @Mock
    private ResultSet resultSet;

    @Before
    public void init() {
        dappRepository = new DappRepository(testURL);
    }

    @Test
    public void insertDappsTestReplace() throws NoQueryPossibleException {
        ArrayList<Dapp> listToAdd = new ArrayList<>();
        Dapp a = new Dapp(1, "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
        Dapp b = new Dapp(1, "a", "b", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
        Dapp c = new Dapp(2, "a", "c", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
        listToAdd.add(a);
        listToAdd.add(b);
        listToAdd.add(c);
        dappRepository.insertDapps(listToAdd);
        assertEquals(dappRepository.findAllDapps().get(0).getLocatie(), "b");
        assertEquals(dappRepository.findAllDapps().get(1).getLocatie(), "c");
    }

    @Test
    public void insertDappsTestIfAdded() throws NoQueryPossibleException {
        ArrayList<Dapp> listToAdd = new ArrayList<>();
        Dapp d = new Dapp(1, "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
        listToAdd.add(d);
        dappRepository.insertDapps(listToAdd);
        assertFalse(dappRepository.findAllDapps().isEmpty());
        assertEquals(dappRepository.findAllDapps().get(0).getTitel(), "a");
    }

    @Test
    public void findDappByTitleTest() throws NoQueryPossibleException {
        ArrayList<Dapp> listToAdd = new ArrayList<>();
        Dapp g = new Dapp(3, "c", "c", "c", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a");
        listToAdd.add(g);
        dappRepository.insertDapps(listToAdd);
        assertEquals(dappRepository.findDappById(3).getLocatie(), "c");
    }

    @Test
    public void findAllTestResultNotEmpty() throws NoQueryPossibleException {
        dappRepository = new DappRepository();
        assertFalse(dappRepository.findAllDapps().isEmpty());
    }

    @Test
    public void insertDappsTestExceptionThrown() throws SQLException, NoQueryPossibleException {
        dappRepository = new DappRepository();
        Dapp d = new Dapp();
        ArrayList<Dapp> testList = new ArrayList<>();
        testList.add(d);
        dappRepository.insertDapps(testList);
    }

    @Test
    public void CreateDappTest() throws SQLException, NoQueryPossibleException {
        when(resultSet.getString("Title")).thenReturn("Koffiemachine");
        Dapp result = dappRepository.createDapp(resultSet);
        assertEquals("Koffiemachine", result.getTitel());
    }

    @Test(expected = NoQueryPossibleException.class)
    public void CreateDappTestExceptionThrown() throws NoQueryPossibleException, SQLException {
        dappRepository = new DappRepository();
        when(resultSet.getString("Title")).thenThrow(SQLException.class);

        dappRepository.createDapp(resultSet);
        verify(resultSet, times(1)).getString("Title");

    }

}
