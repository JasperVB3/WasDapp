package domain;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class FileHandlerTest {

    FileHandler fileHandler = new FileHandler();
    ArrayList<File> files;
    String path = "Input";
    File f = new File(path + "\\test.txt");
    File c = new File(path + "\\test.csv");

    @Test
    public void testGetAllFiles() throws Exception {
        files = fileHandler.getAllFiles();
        assertNotNull(files);

    }

    @Test
    public void testMoveCorrectFile() throws Exception {
        c.getParentFile().mkdirs();
        c.createNewFile();
        fileHandler.moveCorrectFile(c);
        files = fileHandler.getAllFiles();
        assertFalse(files.contains(c));
    }

    @Test
    public void testMoveFaultyFile() throws Exception {
        f.getParentFile().mkdirs();
        f.createNewFile();
        fileHandler.moveFaultyFile(f);
        files = fileHandler.getAllFiles();
        assertFalse(files.contains(f));
    }

}
