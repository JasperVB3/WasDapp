package domain;

import Exceptions.NoQueryPossibleException;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import repositories.DappRepository;
import repositories.OpenCSVReader;

@RunWith(MockitoJUnitRunner.class)
public class FileProcessorTest {
    
    FileProcessor fileProcessor;
    FileHandler fileHandler = new FileHandler();
    OpenCSVReader openCSVReader = new OpenCSVReader();
    ArrayList<File> files;
    String path = "Input";
    File a = new File(path + "\\test.txt");
    File b = new File(path + "\\test.csv");
    File c = new File(path + "\\test.json");
    Dapp dapp = new Dapp();
    ArrayList<Dapp> dapps;

    @Mock
    Repository mockRepository;
    
    @Before
    public void init(){
        fileProcessor = new FileProcessor(mockRepository); 
    }
     
    @Test
    public void testProcessInputFolderGetErrMsg() throws Exception {
        a.getParentFile().mkdirs();
        a.createNewFile();
        fileProcessor.processInputFolder();
        assertEquals(fileProcessor.getErrMsg(), "Not a CSV or JSON file.");
    }

    @Test
    public void testProcessInputFolderCSVMoveFaultyFile() throws Exception {
        b.getParentFile().mkdirs(); 
        b.createNewFile();
        files = fileHandler.getAllFiles();
        assertTrue(files.contains(b));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains(b));
    }

    @Test
    public void testProcessInputFolderCSVMoveCorrectFile() throws Exception {
        Files.copy(Paths.get("Processed\\WasDapp.csv"), Paths.get("Input\\WasDapp.csv"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("Wasdapp.csv"));
    }
    
    @Test
    public void testProcessInputFolderJSONMoveFaultyFile() throws Exception {
        c.getParentFile().mkdirs(); 
        c.createNewFile();
        files = fileHandler.getAllFiles();
        assertTrue(files.contains(c));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains(c));
    } 
    
    @Test
    public void testProcessInputFolderJSONPrintJSONMoveCorrectFile() throws Exception {
        Files.copy(Paths.get("Processed\\WasDappPrintJSON.json"), Paths.get("Input\\WasDappPrintJSON.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappPrintJSON.json"));
    }
    
    @Test
    public void testProcessInputFolderJSONPrintPDFMoveCorrectFile() throws Exception {
        Files.copy(Paths.get("Processed\\WasDappPrintPDF.json"), Paths.get("Input\\WasDappPrintPDF.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappPrintPDF.json"));
    }
    
    @Test
    public void testProcessInputFolderJSONInsertMoveCorrectFile() throws Exception {
        Files.copy(Paths.get("Processed\\WasDappInsert.json"), Paths.get("Input\\WasDappInsert.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappInsert.json"));
    }
    
    @Test
    public void testProcessInputFolderJSONDeleteMoveCorrectFile() throws Exception {
        Files.copy(Paths.get("Processed\\WasDappDelete.json"), Paths.get("Input\\WasDappDelete.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappDelete.json"));
    }
    
    @Test
    public void testProcessInputFolderJSONWrongModificationMoveFaultyFile() throws Exception {
        Files.copy(Paths.get("Rejected\\WasDappGeenBewerking.json"), Paths.get("Input\\WasDappGeenBewerking.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappGeenBewerking.json"));
    }
    
    @Test
    public void testProcessInputFolderJSONWrongOutputMoveFaultyFile() throws Exception {
        Files.copy(Paths.get("Rejected\\WasDappFouteOutput.json"), Paths.get("Input\\WasDappFouteOutput.json"));
        fileProcessor.processInputFolder();
        files = fileHandler.getAllFiles();
        assertFalse(files.contains("WasDappFouteOutput.json"));
    }
}
