package domain;

import org.junit.Test;
import static org.junit.Assert.*;

public class ReadConfigFileTest {

    ReadConfigFile config = new ReadConfigFile();

    @Test
    public void testGetInputFolder() {
        String output = config.getInputFolder();
        assertEquals(output, "Input");
    }

    @Test
    public void testGetJSONOutputFolder() {
        String output = config.getJSONOutputFolder();
        assertEquals(output, "JSON Files");
    }

    @Test
    public void testGetPDFOutputFolder() {
        String output = config.getPDFOutputFolder();
        assertEquals(output, "PDF Files");
    }

    @Test
    public void testGetTime() {
        int output = config.getTime();
        assertEquals(output, 20);
    }

    @Test
    public void testGetProcessedFolder() {
        String output = config.getProcessedFolder();
        assertEquals(output, "Processed");
    }

    @Test
    public void testGetRejectedFolder() {
        String output = config.getRejectedFolder();
        assertEquals(output, "Rejected");
    }

    @Test
    public void testGetErrorFolder() {
        String output = config.getErrorFolder();
        assertEquals(output, "Errors");
    }

    @Test
    public void testDeleteLogsAfterDays() {
        int output = config.getDeleteErrorLogsAfterDays();
        assertEquals(output, 14);
    }

}
