package PDFCreation;

/**
 *
 * @author JVDBG19
 */

import Exceptions.Demo;
import GenerateQR.Dynamic_QR;
import domain.Dapp;
import domain.ReadConfigFile;
import java.awt.Image;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.imageio.ImageIO;
import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.visible.PDFTemplateCreator;
import org.apache.pdfbox.util.Matrix;

public class PDFBox {
    
    
    String[] wrT = null;
    String omschrijving = null;
     
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    ReadConfigFile readConfigFile = new ReadConfigFile();
    private Image image;
    private String errMsg;
    Dynamic_QR qr = new Dynamic_QR();
    
    public String getErrMsg() {
        return errMsg;
    }
    
public void createAllPDFs(ArrayList<Dapp> dappList) throws Exception{
        for(Dapp d: dappList){
            createObjectPDF(d.getId(),d.getLocatie(),d.getTitel(),d.getOmschrijving());
        }
        createFullPDF(dappList);
    }
public void createObjectPDF(int id, String locatie, String title, String omschrijving) throws Exception {
      try{
      PDDocument document = new PDDocument();     
      PDPage page = new PDPage(PDRectangle.A4);
      page.setRotation(90);
      PDRectangle pageSize = page.getMediaBox();
      float pageWidth = pageSize.getWidth();
      
      document.addPage(page);
      
      PDImageXObject pdImage = PDImageXObject.createFromFile("QRCODE\\" + id + ".png", document);
      PDPageContentStream contentStream = new PDPageContentStream(document, page);
      contentStream.transform(new Matrix(0, 1, -1, 0, pageWidth, 0));
      contentStream.drawImage(pdImage, 600, 40);

      contentStream.beginText();
      contentStream.setFont(PDType1Font.COURIER, 30);
      contentStream.newLineAtOffset(50, 500);
      contentStream.showText(title);
      contentStream.endText();
      
      contentStream.beginText();
      contentStream.setFont(PDType1Font.HELVETICA, 30);
      contentStream.newLineAtOffset(600, 500);
      contentStream.showText("WasDapp");
      contentStream.endText();
      
    wrT = WordUtils.wrap(omschrijving, 100).split("\\r?\\n");
    for (int i=0; i< wrT.length; i++) {
      contentStream.beginText();
      contentStream.setFont(PDType1Font.HELVETICA, 12);
      contentStream.newLineAtOffset(165,300-i*15);
      omschrijving = wrT[i];
      contentStream.showText(omschrijving);
      contentStream.endText(); 
      }
    
      contentStream.beginText();
      contentStream.setFont(PDType1Font.COURIER, 20);
      contentStream.newLineAtOffset(50, 100);
      contentStream.showText(locatie);
      contentStream.endText();
      contentStream.close();

      document.save(readConfigFile.getPDFOutputFolder() + "\\" + id + ".pdf");
      document.close();            
      }catch(Exception e){
          errMsg = "Can't create PDF file, ID might not exist in database.";
            System.err.println("Error logged to folder");
            LOGGER.error(errMsg);
      }
   }  

public void createFullPDF(ArrayList<Dapp> dapps) {
        try {
            ReadConfigFile config = new ReadConfigFile();
            PDDocument doc = new PDDocument();
            PDFMergerUtility PDFmerger = new PDFMergerUtility();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy H.m.s");
            String time = format.format(new Date());
            PDFmerger.setDestinationFileName(config.getPDFOutputFolder() + "\\AllObjects " + time + ".pdf");
            for (Dapp d : dapps) {
                File file = new File(config.getPDFOutputFolder() + "\\" + d.getId() + ".pdf");
                doc = PDDocument.load(file); 
                PDFmerger.addSource(file);
                doc.close();
            }
                PDFmerger.mergeDocuments();
        } catch (Exception e) {
            errMsg = "Can't create full PDF file!";
            System.err.println("Error logged to folder");
            LOGGER.error(errMsg);
        }
    
    }
}

  
