package PDFCreation;

import Exceptions.Demo;
import GenerateQR.Dynamic_QR;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import domain.Dapp;
import domain.ReadConfigFile;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;

public class PDFCreator {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    ReadConfigFile readConfigFile = new ReadConfigFile();
    private String errMsg;
    
    Dynamic_QR qr = new Dynamic_QR();
    
    public String getErrMsg() {
        return errMsg;
    }
    
    public void createAllPDFs(ArrayList<Dapp> dappList) throws Exception{
        for(Dapp d: dappList){
            createObjectPDF(d.getId(), d.getTitel());
        }
        createFullPDF(dappList);
    }

    public void createObjectPDF(int id, String title) {
        qr.CreateQRs();
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(readConfigFile.getPDFOutputFolder() + "\\" + title + ".pdf"));

            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Paragraph paragraph = new Paragraph(title, font);

            document.add(paragraph);

            Image img = Image.getInstance("QRCODE\\" + id + ".png");
            document.add(img);
            document.close();
        } catch (Exception e) {
            errMsg = "Can't create PDF file.";
            System.err.println("Error logged to folder");
            LOGGER.error(errMsg);
        }
    }

    public void createFullPDF(ArrayList<Dapp> dapps) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(readConfigFile.getPDFOutputFolder() + "\\AllObjects.pdf"));
            Paragraph paragraph = null;
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            for (Dapp d : dapps) {
                paragraph = new Paragraph(d.getTitel(), font);
                document.add(paragraph);
                
                Image img = Image.getInstance("QRCODE\\" + d.getId() + ".png");
                document.add(img);
                document.newPage();
            }
            document.close();
        } catch (Exception e) {
            errMsg = "Can't create full PDF file!";
            System.err.println("Error logged to folder");
            LOGGER.error(errMsg);
        }
    
    }
}
