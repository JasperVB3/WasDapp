package repositories;

import Exceptions.Demo;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import domain.Dapp;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;

public class OpenCSVReader {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());

    private String errMsg;

    public String getErrMsg() {
        return errMsg;
    }
    
    public void setErrMsg(String errMsg)
    {
        this.errMsg = errMsg;
    }

    public ArrayList<Dapp> createDappListFromCSV(String path) {

        try (
                Reader reader = Files.newBufferedReader(Paths.get(path));
                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(0).build();) {
            ArrayList<Dapp> dappList = new ArrayList<>();
            String[] nextRecord;

            String[] potentialHeaders = csvReader.readNext();

            if (!(potentialHeaders[0].equals("ID"))) {
                errMsg = "File without headers!";
                System.out.println("Error logged to folder.");
                LOGGER.error(errMsg);
                throw new Exception();
            }

            while ((nextRecord = csvReader.readNext()) != null) {
                Dapp dapp = new Dapp();
                if (!nextRecord[1].isEmpty()) {
                    if (!nextRecord[0].isEmpty()) {
                        dapp.setId(Integer.parseInt(nextRecord[0]));
                    } else {
                        dapp.setId(0);
                    }
                    dapp.setTitel(nextRecord[1]);
                    dapp.setLocatie(nextRecord[2]);
                    dapp.setStraat(nextRecord[3]);
                    dapp.setNummer(nextRecord[4]);
                    dapp.setPostcode(nextRecord[5]);
                    dapp.setGemeente(nextRecord[6]);
                    dapp.setLand(nextRecord[7]);
                    dapp.setOmschrijving(nextRecord[8]);
                    dapp.setWikipediaLink(nextRecord[9]);
                    dapp.setWebsite(nextRecord[10]);
                    dapp.setTelefoon(nextRecord[11]);
                    dapp.setEmail(nextRecord[12]);
                    dapp.setPrijs(nextRecord[13]);
                    dapp.setPersoon(nextRecord[14]);

                    for (int i = 0; i < 15; i++) {
                        if (nextRecord[i].length() > 256) {
                            errMsg = "Please keep the maximum length of the values to no more than 256.";
                            System.out.println("Error logged to folder.");
                            LOGGER.error(errMsg);
                            throw new Exception();
                        }
                    }

                    dappList.add(dapp);

                } else {
                    dappList.removeAll(dappList);
                    errMsg = "Empty title in csv-file found.";
                    System.out.println("Error logged to folder.");
                    LOGGER.error(errMsg);
                    throw new Exception();
                }

            }
            return dappList;
        } catch (Exception e) {
            System.out.println("Error logged to folder.");
            LOGGER.error("\nPlease make sure your csv file is valid:\nSeperate data fields with a comma.\nKeep each record on a seperate line.\nDo not follow the last record in a file with a carriage return.\nEach row needs an equal amount of colums.\nMake sure there is a title.\nMake sure the headers are correct.");
            return null;
        }
    }
}
