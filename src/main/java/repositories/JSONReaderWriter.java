package repositories;

import Exceptions.Demo;
import GenerateQR.Dynamic_QR;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Dapp;
import domain.FullJSON;
import domain.ReadConfigFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.codehaus.plexus.util.Base64;

public class JSONReaderWriter {
    
    Dynamic_QR qr = new Dynamic_QR();
    ReadConfigFile config = new ReadConfigFile();
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    ObjectMapper mapper = new ObjectMapper();
    private String errMsg;
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy H.m.s");

    public FullJSON createFullJSON(String filePath) throws IOException {
        FullJSON fullJSON = null;

        try {
            fullJSON = mapper.readValue(new File(filePath), FullJSON.class);
        } catch (Exception e) {
        }

        return fullJSON;
    }

    public void writeJSONToFile(ArrayList<Dapp> dapps) {
        try {
            for (Dapp d : dapps) {
                File f = new File("QRCODE\\" + d.getId() + ".png");
                String encodedfile = null;
                try {
                    FileInputStream fileInputStreamReader = new FileInputStream(f);
                    byte[] bytes = new byte[(int) f.length()];
                    fileInputStreamReader.read(bytes);
                    encodedfile = Base64.encodeBase64(bytes).toString();
                    d.setQRCode(encodedfile);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            mapper.writeValue(new File(config.getJSONOutputFolder() + "\\" + format.format(new Date()) + ".json"), dapps);
        } catch (Exception e) {
            e.printStackTrace();
//            errMsg = "Can't write to JSON file.";
//            System.err.println("Error logged to error folder.");
//            LOGGER.error(errMsg);
        }
    }

    public String getErrMsg() {
        return errMsg;
    }

}
