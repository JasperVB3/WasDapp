package repositories;

import Exceptions.NoQueryPossibleException;
import domain.Dapp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Exceptions.*;
import domain.Repository;
import org.apache.logging.log4j.LogManager;

public class DappRepository implements Repository{

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    private static final String LOGIN = "root";
    private static final String PASSWORD = "root";
    private static String DRIVER = "com.mysql.jdbc.Driver";
    private static String URL = "jdbc:mysql://localhost:3306/wasdapp?autoReconnect=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static final String ID = "ID";

    private static final String TITLE = "Title";
    private static final String LOCATIE = "Locatie";
    private static final String STRAAT = "Straat";
    private static final String NUMMER = "Nummer";
    private static final String POSTCODE = "Postcode";
    private static final String GEMEENTE = "Gemeente";
    private static final String LAND = "Land";
    private static final String OMSCHRIJVING = "Omschrijving";
    private static final String WIKIPEDIALINK = "Wikipedia_link";
    private static final String WEBSITE = "Website";
    private static final String TELEFOON = "Telefoon";
    private static final String EMAIL = "E_mail";
    private static final String PRIJS = "Prijs";
    private static final String PERSOON = "Persoon";

    public DappRepository() {
        this(URL);
    }

    //only for adding a test url
    protected DappRepository(String url) {
        URL = url;
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, LOGIN, PASSWORD);
    }

    public Dapp findDappById(int id) throws NoQueryPossibleException {
        Dapp dapp = new Dapp();
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM wasdapp WHERE " + ID + " = '" + id + "'");
            ResultSet resultSet = pstatement.executeQuery();
            if (resultSet.next()) {
                dapp = createDapp(resultSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error Message Logged !");
        }
        return dapp;
    }

    public ArrayList<Dapp> findAllDapps() throws NoQueryPossibleException {
        ArrayList<Dapp> dappList = null;
        try (Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM wasdapp");
            ResultSet resultSet = pstatement.executeQuery();
            dappList = new ArrayList<>();
            while (resultSet.next()) {
                dappList.add(createDapp(resultSet));
            }
        } catch (Exception e) {
            System.err.println("Error logged to folder");
            LOGGER.error("Couldn't connect to the database / Database is invalid.");

        }
        return dappList;
    }

    public boolean insertDapps(ArrayList<Dapp> dappList) throws NoQueryPossibleException {
        try (Connection connection = createConnection()) {
            ArrayList<Integer> existingIds = new ArrayList<>();
            String action = "";
            for (Dapp d : findAllDapps()) {
                existingIds.add(d.getId());

            }
            for (Dapp d : dappList) {
                if (existingIds.contains(d.getId())) {
                    action = "REPLACE";
                } else {
                    action = "INSERT";
                }
                PreparedStatement pstatement = connection.prepareStatement(action + " INTO wasdapp(ID, Title, Locatie, Straat, Nummer, Postcode, Gemeente, Land, Omschrijving, Wikipedia_link, Website, Telefoon, E_mail, Prijs, Persoon) "
                        + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                pstatement.setString(1, Integer.toString(d.getId()));
                pstatement.setString(2, d.getTitel());
                pstatement.setString(3, d.getLocatie());
                pstatement.setString(4, d.getStraat());
                pstatement.setString(5, d.getNummer());
                pstatement.setString(6, d.getPostcode());
                pstatement.setString(7, d.getGemeente());
                pstatement.setString(8, d.getLand());
                pstatement.setString(9, d.getOmschrijving());
                pstatement.setString(10, d.getWikipediaLink());
                pstatement.setString(11, d.getWebsite());
                pstatement.setString(12, d.getTelefoon());
                pstatement.setString(13, d.getEmail());
                pstatement.setString(14, d.getPrijs());
                pstatement.setString(15, d.getPersoon());

                pstatement.executeUpdate();
            }

        } catch (SQLException e) {
            System.err.println("Error logged to folder");
            LOGGER.error("Couldn't insert into the database");
            return false;
        }
        return true;
    }

    public Dapp createDapp(ResultSet resultSet) throws NoQueryPossibleException {
        Dapp dapp = null;
        try {
            dapp = new Dapp();
            dapp.setId(resultSet.getInt(ID));
            dapp.setTitel(resultSet.getString(TITLE));
            dapp.setLocatie(resultSet.getString(LOCATIE));
            dapp.setStraat(resultSet.getString(STRAAT));
            dapp.setNummer(resultSet.getString(NUMMER));
            dapp.setPostcode(resultSet.getString(POSTCODE));
            dapp.setGemeente(resultSet.getString(GEMEENTE));
            dapp.setLand(resultSet.getString(LAND));
            dapp.setOmschrijving(resultSet.getString(OMSCHRIJVING));
            dapp.setWikipediaLink(resultSet.getString(WIKIPEDIALINK));
            dapp.setWebsite(resultSet.getString(WEBSITE));
            dapp.setTelefoon(resultSet.getString(TELEFOON));
            dapp.setEmail(resultSet.getString(EMAIL));
            dapp.setPrijs(resultSet.getString(PRIJS));
            dapp.setPersoon(resultSet.getString(PERSOON));
        } catch (SQLException ex) {
            System.err.println("Error logged to folder");
            LOGGER.error("Couldn't create object.");
            throw new NoQueryPossibleException();
        }
        return dapp;
    }
    
    public void deleteDapps(ArrayList<Dapp> dapps) throws NoQueryPossibleException{
        for (Dapp d : dapps){
            int id = d.getId();
            deleteDappbyId(id);
        }
    }
    
    public void deleteDappbyId(int i) throws NoQueryPossibleException {
        try(Connection connection = createConnection()) {
            PreparedStatement pstatement = connection.prepareStatement("Delete from wasdapp where id = ?");
            pstatement.setString(1, Integer.toString(i));
            pstatement.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            throw new NoQueryPossibleException();
        }
    }

}
