
package domain;

import Exceptions.NoQueryPossibleException;
import java.sql.ResultSet;
import java.util.ArrayList;


public interface Repository {
    
    public Dapp findDappById(int id) throws NoQueryPossibleException;
    public boolean insertDapps(ArrayList<Dapp> dappList)  throws NoQueryPossibleException;
    public ArrayList<Dapp> findAllDapps()  throws NoQueryPossibleException;
    public Dapp createDapp(ResultSet resultSet) throws NoQueryPossibleException;
    public void deleteDapps(ArrayList<Dapp> dapps) throws NoQueryPossibleException;
    public void deleteDappbyId(int i) throws NoQueryPossibleException;
    
    
}
