package domain;

import Exceptions.Demo;
import Exceptions.NoQueryPossibleException;
import GenerateQR.Dynamic_QR;
import PDFCreation.PDFBox;
import PDFCreation.PDFCreator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import repositories.DappRepository;
import repositories.JSONReaderWriter;
import repositories.OpenCSVReader;

public class FileProcessor {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());

    PDFBox pdfBox;
    FileHandler fileHandler;
    OpenCSVReader openCSVReader;
    JSONReaderWriter jsonReader;
    Repository dappRepository;
    Dynamic_QR qr;

    String path;
    private String errMsg;

    public FileProcessor(Repository dappRepository) {
        this.dappRepository = dappRepository;
    }

    public void processInputFolder() throws IOException, NoQueryPossibleException, Exception {
        fileHandler = new FileHandler();
        dappRepository = new DappRepository();

        for (File f : fileHandler.getAllFiles()) {
            path = f.toString();
            if (path.endsWith(".csv")) {
                processCSV(path, f);
            } else if (path.endsWith(".json")) {
                processJSON(path, f);
            } else {
                errMsg = "Not a CSV or JSON file.";
                LOGGER.error(errMsg);
                fileHandler.moveFaultyFile(f);
            }
        }
    }

    public void processCSV(String path, File f) throws Exception {
        openCSVReader = new OpenCSVReader();
        ArrayList<Dapp> dappsFromInput;
        dappsFromInput = openCSVReader.createDappListFromCSV(path);
        if (dappsFromInput != null) {
            dappRepository.insertDapps(filterDappsToMatchDB(dappsFromInput));
            fileHandler.moveCorrectFile(f);
        } else {
            fileHandler.moveFaultyFile(f);
        }
    }

    public void processJSON(String path, File f) throws Exception {
        jsonReader = new JSONReaderWriter();
        FullJSON fullJSON = jsonReader.createFullJSON(path);

        if (!(fullJSON == null)) {
            ArrayList<Dapp> dappsFromInput;
            dappsFromInput = fullJSON.getDapps();
            switch (fullJSON.getBewerking()) {
                case "insert":
                    dappRepository.insertDapps(filterDappsToMatchDB(dappsFromInput));
                    fileHandler.moveCorrectFile(f);
                    break;

                case "print":
                    qr = new Dynamic_QR();
                    qr.CreateQRs();
                    ArrayList<Dapp> dappsToPrint;
                    try{
                    if (fullJSON.getOutput().equals("pdf")) {
                        dappsToPrint = new ArrayList<>();
                        pdfBox = new PDFBox();
                        for(Dapp d : filterDappsToMatchDB(dappsFromInput)){
                            if(d.getId() != 0){
                            dappsToPrint.add(dappRepository.findDappById(d.getId()));
                            }
                        }
                        pdfBox.createAllPDFs(dappsToPrint);
                        fileHandler.moveCorrectFile(f);

                    } else if (fullJSON.getOutput().equals("json")) {
                        dappsToPrint = new ArrayList<>();
                        
                        for(Dapp d : filterDappsToMatchDB(dappsFromInput)){
                            if(d.getId() != 0){
                            dappsToPrint.add(dappRepository.findDappById(d.getId()));
                            }
                        }
                        jsonReader.writeJSONToFile(dappsToPrint);
                        fileHandler.moveCorrectFile(f);

                    } else {
                        errMsg = "The 'output' field in the json file was not 'pdf' or 'json'.";
                        LOGGER.error(errMsg);
                        fileHandler.moveFaultyFile(f);

                    }
                    
                    }catch(Exception e){
                        errMsg = "The 'output' field in the json file was not 'pdf' or 'json'.";
                        LOGGER.error(errMsg);
                        fileHandler.moveFaultyFile(f);
                    }
                    break;
                    
                case "delete":
                    dappRepository.deleteDapps(filterDappsToMatchDB(dappsFromInput));
                    fileHandler.moveCorrectFile(f);
                    break;
                default:
                    errMsg = "Please add a correct command to your JSON document!";
                    LOGGER.error(errMsg);
                    fileHandler.moveFaultyFile(f);
            }

        } else {
            errMsg = "The JSON file is not correctly structured!";
            LOGGER.error(errMsg);
            fileHandler.moveFaultyFile(f);
        }
    }

    public ArrayList<Dapp> filterDappsToMatchDB(ArrayList<Dapp> dappsFromInput) throws Exception {
        ArrayList<Dapp> dappsInDB = dappRepository.findAllDapps();
        List<Integer> idsInDB = dappsInDB.stream()
                .map(Dapp::getId)
                .collect(Collectors.toList());

        String rejectedIds = "";
        ArrayList<Dapp> dappToRemove = new ArrayList<>();
        boolean idsRejected = false;
        for (int i = 0; i < dappsFromInput.size(); i++) {
            int currentID = dappsFromInput.get(i).getId();
            if ((!(idsInDB.contains(currentID))) 
                    && (currentID!=0)) {
                rejectedIds += (dappsFromInput.get(i).getId() + ", ");
                dappToRemove.add(dappsFromInput.get(i));
                idsRejected = true;
            }
        }
        dappsFromInput.removeAll(dappToRemove);
        if (idsRejected == true) {
            errMsg = "The file was handled correctly but the following ids were not found in the database: " + rejectedIds + ". These items were not handled or altered in any way.";
            LOGGER.error(errMsg);
        }
        return dappsFromInput;
    }

    public String getErrMsg() {
        return errMsg;
    }

}
