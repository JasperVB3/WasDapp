package domain;

import java.util.ArrayList;

public class FullJSON {

    private String bewerking, output;
    private ArrayList<Dapp> dapps;

    public FullJSON() {
    }

    public FullJSON(String bewerking, String output, ArrayList<Dapp> dapps) {
        this.bewerking = bewerking;
        this.output = output;
        this.dapps = dapps;
    }

    public String getBewerking() {
        return bewerking;
    }

    public void setBewerking(String bewerking) {
        this.bewerking = bewerking;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public ArrayList<Dapp> getDapps() {
        return dapps;
    }

    public void setDapps(ArrayList<Dapp> dapps) {
        this.dapps = dapps;
    }

    @Override
    public String toString() {
        return "FullJSON{" + "bewerking=" + bewerking + ", output=" + output + ", dapps=" + dapps + '}';
    }

}
