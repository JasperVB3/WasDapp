
package domain;

import java.io.IOException;
import java.nio.file.*;
import repositories.DappRepository;



public class Watcher {
    
Repository dappRepository = new DappRepository();
ReadConfigFile config = new ReadConfigFile();
FileProcessor fp = new FileProcessor(dappRepository);
FileHandler fh = new FileHandler();
    public void watch() throws IOException, InterruptedException, Exception {
        WatchService watchService
                = FileSystems.getDefault().newWatchService();

        Path path = Paths.get(config.getInputFolder());

        path.register(        
                watchService,
                StandardWatchEventKinds.ENTRY_CREATE);

        WatchKey key;
        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                System.out.println(
                        "Event kind:" + event.kind()
                        + ". File affected: " + event.context() + ".");
                fp.processInputFolder();
                fh.deleteErrorFilesAfter14Days();
                
            }
            key.reset();
        }
    }
}
