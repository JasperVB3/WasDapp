package domain;

import Exceptions.Demo;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.codehaus.plexus.util.FileUtils;

public class FileHandler {
    
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());
    
    ReadConfigFile config = new ReadConfigFile();
    String path = "";
    String errMsg;

    public static void goThroughDirectories(String path) {

    }

    public ArrayList<File> getAllFiles() throws IOException {        
        try{
        ArrayList<File> allFiles = new ArrayList<>();
        File input = new File(config.getInputFolder());
        if (input.isDirectory()) {
            goThroughDirectories(path);
        }
        {
            File[] content = input.listFiles();
            for (int i = 0; i < content.length; i++) {
                allFiles.add(content[i]);
            }
        }
        return allFiles;
        
        }catch(Exception e){
            errMsg = "Please put only valid files in the Input folder.";
            LOGGER.error(errMsg);
            return null;
        }
    }

    public void moveCorrectFile(File file) throws IOException {
        path = file.toString();
        File output = new File(config.getProcessedFolder());
        Files.move(Paths.get(path), Paths.get(output + "\\" + file.getName()), StandardCopyOption.REPLACE_EXISTING);
    }

    public void moveFaultyFile(File file) throws IOException {
        try{
        path = file.toString();
        File output = new File(config.getRejectedFolder());
        Files.move(Paths.get(path), Paths.get(output + "\\" + file.getName()), StandardCopyOption.REPLACE_EXISTING);
        }catch(Exception e){
            errMsg = "Please check your Input folder, some items may not have been processed.";
            LOGGER.error(errMsg);      
        }
    }

    public void deleteErrorFilesAfter14Days() {
        File errors = new File(config.getErrorFolder());
        long now = System.currentTimeMillis();
        long oneDay = 1000 * 60 * 60 * 24;
        long amountDays = config.getDeleteErrorLogsAfterDays() * oneDay;
        if (errors.isDirectory()) {
            File[] content = errors.listFiles();
            for (int i = 0; i < content.length; i++) {
                long diff = now - content[i].lastModified();
                if (diff > amountDays) {
                    content[i].delete();
                }
            }
        }
    }
}
