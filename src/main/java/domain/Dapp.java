package domain;

import org.apache.pdfbox.pdmodel.PDDocument;

public class Dapp {
    
    private int id;
    private String titel,locatie,straat,nummer,postcode,gemeente,land,omschrijving,wikipediaLink,website,telefoon,email,prijs,persoon;
    private String QRCode;

    public Dapp() {
    }

    public Dapp(int id, String titel, String locatie, String straat, String nummer, String postcode, String gemeente, String land, String omschrijving, String wikipediaLink, String website, String telefoon, String email, String prijs, String persoon) {
        this.id = id;
        this.titel = titel;
        this.locatie = locatie;
        this.straat = straat;
        this.nummer = nummer;
        this.postcode = postcode;
        this.gemeente = gemeente;
        this.land = land;
        this.omschrijving = omschrijving;
        this.wikipediaLink = wikipediaLink;
        this.website = website;
        this.telefoon = telefoon;
        this.email = email;
        this.prijs = prijs;
        this.persoon = persoon;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public String getNummer() {
        return nummer;
    }

    public void setNummer(String nummer) {
        this.nummer = nummer;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getWikipediaLink() {
        return wikipediaLink;
    }

    public void setWikipediaLink(String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrijs() {
        return prijs;
    }

    public void setPrijs(String prijs) {
        this.prijs = prijs;
    }

    public String getPersoon() {
        return persoon;
    }

    public void setPersoon(String persoon) {
        this.persoon = persoon;
    }

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }
    
    

    @Override
    public String toString() {
        return "\nID: " + id + "\nTitel: " + titel +"\nLocatie: " + locatie +"\nStraat: " + straat + "\nNummer: " + nummer + "\nPostcode: " + postcode + "\nGemeente: " + gemeente + "\nLand: " + land + "\nOmschrijving: " + omschrijving + "\nWikipediaLink: " + wikipediaLink + "\nWebsite: " + website + "\nTelefoon: " + telefoon + "\nEmail: " + email + "\nPrijs: " + prijs + "\nPersoon: " + persoon + "\n=================================";
    }

    PDDocument getPDDocument() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    PDDocument getDocument() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
