package domain;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import Exceptions.NoQueryPossibleException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, NoQueryPossibleException, DocumentException, FileNotFoundException, BadElementException, BadElementException, URISyntaxException, InterruptedException, Exception {
        Watcher watcher = new Watcher();
        watcher.watch();
    }
}

