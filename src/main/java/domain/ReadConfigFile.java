package domain;

import java.util.ResourceBundle;

public class ReadConfigFile {

    private static ResourceBundle rb = ResourceBundle.getBundle("config");

    public String getInputFolder() {
        String inputFolder = rb.getString("inputfolder");
        return inputFolder;
    }

    public String getJSONOutputFolder() {
        String JSONOutputFolder = rb.getString("JSONoutputfolder");
        return JSONOutputFolder;
    }

    public String getPDFOutputFolder() {
        String PDFOutputFolder = rb.getString("PDFoutputfolder");
        return PDFOutputFolder;
    }

    public int getTime() {
        int checkFolderTime = Integer.parseInt(rb.getString("CheckFolderTime"));
        return checkFolderTime;
    }

    public String getProcessedFolder() {
        String processedFolder = rb.getString("ProcessedFolder");
        return processedFolder;
    }

    public String getRejectedFolder() {
        String rejectedFolder = rb.getString("RejectedFolder");
        return rejectedFolder;
    }

    public String getErrorFolder() {
        String errorFolder = rb.getString("ErrorFolder");
        return errorFolder;
    }

    public int getDeleteErrorLogsAfterDays() {
        int deleteErrorLogsAfterDays = Integer.parseInt(rb.getString("DeleteErrorLogsAfterDays"));
        return deleteErrorLogsAfterDays;
    }

}
