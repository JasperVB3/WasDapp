package GenerateQR;

import Exceptions.Demo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;

public class DBConnection {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());

    public Connection getConnection() {
        Connection connection = null;
        System.out.println("Connection called");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/wasdapp?autoReconnect=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
        } catch (ClassNotFoundException e) {
            System.err.println("Error logged to folder");
            LOGGER.error("Couldn't connect to the database");
        } catch (SQLException e) {
            System.err.println("Error logged to folder");
            LOGGER.error("Couldn't connect to the database");
        }
        return connection;
    }
}
