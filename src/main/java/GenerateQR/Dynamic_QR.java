package GenerateQR;
import Exceptions.Demo;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.apache.logging.log4j.LogManager;
import org.codehaus.plexus.util.FileUtils;

public class Dynamic_QR {
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(Demo.class.getName());

	public static void CreateQRs() {
		try {
			DBConnection obj_DBConnection = new DBConnection();
            Connection connection = obj_DBConnection.getConnection();
            String query = "select * from wasdapp";
            Statement stmt = null;
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
                    FileUtils.cleanDirectory("QRCODE");
            while (rs.next()) {
            	Dynamic_QR.generate_qr(rs.getInt("id"),rs.getInt("id"));
            }
		} catch (Exception e) {
			System.err.println("Error logged to folder");
                        LOGGER.error("Couldn't create static QR code");
		}
	}
	public static void generate_qr(int image_name,int qrCodeData) {
        try {
            String filePath = "QRCODE\\"+image_name+".png";
            String charset = "UTF-8"; // or "ISO-8859-1"
            Map < EncodeHintType, ErrorCorrectionLevel > hintMap = new HashMap < EncodeHintType, ErrorCorrectionLevel > ();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BitMatrix matrix = new MultiFormatWriter().encode(
                new String(Integer.toString(qrCodeData).getBytes(charset), charset),
                BarcodeFormat.QR_CODE, 200, 200, hintMap);
            MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath
                .lastIndexOf('.') + 1), new File(filePath));
        } catch (Exception e) {
            System.err.println("Error logged to folder");
            LOGGER.error("Failed to create QR codes.");
        }
    }
}
