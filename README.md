How to install:
1.Extract all files into a folder of your choice(Not in Program Files or Program Filesx86).  

1.Install MySQL Workbench (https://dev.mysql.com/downloads/workbench/).

2.Use MySQL Workbench to run the SQL script "WasDapp.sql" to create the database.
   Make sure you use login 'root' and password 'root' as your login.

3.Install Java (https://www.java.com/en/download/).

4.Make sure you have file extensions enabled. 


How to use:
1.Run the program by opening 'WasDapp-8.0.12-jar-with-dependencies.jar'. Now it will run in the background.

2.Drag and drop(copy + paste) your .json or .csv file into the Input folder. 

   Look at the examples in WasDapp/Examples to make sure your file is correctly structured.
   The default folder is called WasDapp/Input, however this can be changed in the config file.

   Correct .csv files will always add data to the database.
   !!!Make sure each field is separated by a comma, even if there is no data to enter into the field(15 fields in total)!!!
   !!!All items must contain a title!!!

   Correct .json files will perform one of three actions, depending on the tag included in 'bewerking', which is mandatory.
	
    -Insert: This will insert the given data into the database. 
	              !!!All items must contain a title!!!
	              If you wish to make a new entry, leave the ID field blank.
	              If you wish to modify data, add the respective ID.
	              These rules also apply to the .csv files

	-Print: This will provide you with either a .json or .pdf file, depending on the tag included in 'output', which is mandatory.
	           To perform a print, you must provide the IDs of the existing items in the database.
	           All data from the respective items will be printed and a QR-code will be included.
	           
	           If your 'output' tag was 'pdf', WasDapp will:
		- Create a PDF document with all items you included
		- Create a seperate PDF for each item. 
		For each item, a QR-code will be included on the page.
		!!!Existing PDF files cannot be updated while opened!!!

	           If your 'output' tag was 'json', WasDapp will:
		- Create a .json file, with all items you included.
		For each item, a QR-code will be included in text format(base64).

	-Delete: This will remove entries from the database that match the ID's you provided in your .json file.

How to modify the config file:
1.Go to WasDapp Folder -> src -> main -> Resources
2.Open the config.properties file with a text editor like notepad.

Extra info:
	If you provide IDs in your file that are not in the database, these IDs will be skipped.
	           