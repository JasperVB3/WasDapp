CREATE DATABASE `wasdapp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
use wasdapp;
CREATE TABLE `wasdapp` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Locatie` varchar(255) DEFAULT NULL,
  `Straat` varchar(255) DEFAULT NULL,
  `Nummer` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `Gemeente` varchar(255) DEFAULT NULL,
  `Land` varchar(255) DEFAULT NULL,
  `Omschrijving` varchar(255) DEFAULT NULL,
  `Wikipedia_link` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `Telefoon` varchar(255) DEFAULT NULL,
  `E_mail` varchar(255) DEFAULT NULL,
  `Prijs` varchar(255) DEFAULT NULL,
  `Persoon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
